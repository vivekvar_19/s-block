**NEURAL NETWORKS**

    > A neural network is a series of algorithms that endeavors to recognize underlying relationships in a set of data through a process that mimics
     the way the human brain operates.

    > In this sense, neural networks refer to systems of neurons, either organic or artificial in nature. Neural networks can adapt to changing input;
     so the network generates the best possible result without needing to redesign the output criteria. 

*INPUT LAYER* :- This layer accepts the input given by the user.

*LAYER 1 or HIDDEN LAYER* :- In this layer, all the computations are performed.

*LAYER 2 or OUTPUT LAYER* :- The output is delivered through this layer.

**TYPES OF NEURAL NETWORKS** 

    - Feed Forward Neural Networks
    - Perceptron
    - Multilayer Perceptron
    - Convolutional Neural Networks
    - Radial Basis Function Neural Networks

A simple image of neural networks :-

![](https://gitlab.com/vivekvar_19/s-block/-/raw/master/images/img.png)

**APPLICATIONS OF NEURAL NETWORKS**

    > Neural Networks are used in various fields and are helped to solve various complex problems. Some of them are :-

    - Image Processing
    - Language Processing and Translation
    - Route Detection
    - Speech Recognition
    - Forecasting

**GRADIENT DESCENT**

    > Gradient Descent is an optimization technique that is used to improve deep learning and neural network-based models by minimizing the cost
     function.

*LEARNING RATE*

    > The amount that the weights are updated during training is referred to as the step size or the learning rate.
     Specifically, the learning rate is a configurable hyperparameter used in the training of neural networks that has a small positive value, often
      in the range between 0.0 and 1.0.

*STEPS TO CALCULATE GRADIENT DESCENT* :-

    1. Given the gradient, calculate the change in the parameters with the learning rate.

    2. Re-calculate the new gradient with the new value of the parameter.

    3. Repeat step 1.

*FORMULA OF GRADIENT DESCENT ALGORITHM*

![](https://gitlab.com/vivekvar_19/s-block/-/raw/master/images/IMG1.jpg)

*TYPES OF GRADIENT DESCENT*

    - Batch Gradient Descent
    - Stochastic gradient descent
    - Mini-batch gradient descent

**BACKPROPAGATION**

    > Backpropagation is the central mechanism by which neural networks learn. It is the messenger telling the network whether or not the net made a
     mistake when it made a prediction.

    > A neural network propagates the signal of the input data forward through its parameters towards the moment of decision, and then backpropagates
     information about the error, in reverse through the network, so that it can alter the parameters.

*STEPS IN BACKPROPAGATION* :-

    1. The network makes a guess about data, using its parameters.

    2. The network’s is measured with a loss function.

    3. The error is backpropagated to adjust the wrong-headed parameters.

*TYPES OF BACKPROPAGATION* :-

    1. Static Backpropagation

    2. Recurrent Backpropagation

*DISADVANTAGES OF BACKPROPAGATION* :-

    - The actual performance of backpropagation on a specific problem is dependent on the input data.
    - Backpropagation can be quite sensitive to noisy data
    - You need to use the matrix-based approach for backpropagation instead of mini-batch.
 