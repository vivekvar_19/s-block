**BRIEF MEANING OF ALL THE FUNCTIONS AND KEYWORDS USED IN REFERENCE MATERIAL**

1. *KERAS LIBRARY* :- This is a library in *PYTHON* and by using it, it's pretty simple to build a CNN model.

2. *MNIST DATASET* :- This a dataset of the *KERAS* library which in turn is a library of *PYTHON*. It consists of 70,000 images of numbers out of which 60,000 are used for training and rest 10,000 are used for testing.

3. *ONE - HOT - ENCODE* :- A one hot encoding is a representation of categorical variables as binary vectors. This first requires that the categorical values be mapped to integer values. Then, each integer value is represented as a binary vector that is all zero values except the index of the integer, which is marked with a 1.

4. *SEQUENTIAL MODEL TYPE* :- The Sequential model is a linear stack of layers. We can create a Sequential model by passing a list of layer instances to the constructor.

5. *RESHAPE FUNCTION* :- *SYNTAX* :- X_train(a, b, c, d).

    Here, a = number of images used to train our model,

    b and c are the shape of images. In, *KERAS* model, shape is generally 28, 28.

    d = 1, meaning that images are greyscale.

6. *ADD FUNCTION* :- This function is used to add layers to our sequential model type.

7. *CONVOLUTIONAL LAYERS* :- They have the property of 'spatial invariance', meaning they learn to recognise image features anywhere in your image. They are the first two layers of our CNN model.

8. *RECTIFIED LINEAR ACTIVATION* :- The rectified linear activation function overcomes the vanishing gradient problem, allowing models to learn faster and perform better. The rectified linear activation is the default activation when developing multilayer Perceptron and convolutional neural networks.

9. *FLATTEN LAYER* :- It serves as a connection between the convolution and dense layers.

10. *DENSE LAYER or OUTPUT LAYER* :- A dense layer is just a regular layer of neurons in a neural network. Each neuron recieves input from all the neurons in the previous layer, thus densely connected.

11. *SOFTMAX ACTIVATION* :- The softmax activation is normally applied to the very last layer in a neural net. The reason why softmax is useful is because it converts the output of the last layer in your neural network into what is essentially a probability distribution.

12. *ADAM OPTIMIZER* :- Adam optimization is a stochastic gradient descent method that is based on adaptive estimation of first-order and second-order moments.

13. *CATEGORIAL_CROSSENTROPY LOSS FUNCTION* :- This is used for handling our loss.  A lower score indicates that the model is performing better.

14. *ACCURACY METRIC* :- It is used to check accuracy score on the validation set when we train the model.

15. *FIT FUNCTION* :- Used to train the model.

16. *PREDICT FUNCTION* :- Used to test the model.
