| **PARAMETERS**  | **AMAZON REKOGNITION**      | **MICROSOFT COGNITIVE SERVICES** |
|   -------       |          -------            |              -------             |
| *CORE FEATURES* | - Object and Scene Analysis | - Face Detection                 |
|                 | - Face Detection            | - Face Verification              |
|                 | - Face Recognition          | - Face Identification            |
|                 | - Face Sentiment Analysis   | - Emotion Detection              |
|*IMAGE SIZE LIMITS*| 5 mb per image or 15 mb per image from S3|4 mb per image|
|*RATE LIMITS*|Not Defined|10 requests per second|
|*PERFORMANCE TESTING (FOR 1000 FILES, 10 FILES AT A TIME)*| | |
|   - AVERAGE| 2.42 sec| 1.11 sec|
| - MINIMUM | 1.03 sec | 0.65 sec|
| - MAXIMUM | 3.73 sec | 5.07 sec|
|*DOMINANT COLORS DETECTION*| Not Allowed | Allowed|
|*LANDMARK DETECTION* | Not Allowed | Allowed|
|*IMAGE FORMATS SUPPORTED* | - JPEG | - JPEG |
| | - PNG | - PNG|
| | | -GIF|
|*VIDEO SPECIFICATIONS* | The supported video file formats   |    Emotion videos limited to 100MB max 
| |                       are MPEG-4 and MOV and the stored    |  (10 - 20 seconds of video at 1080p resolution,
| |                       video must be encoded using the H. 264  | <30 seconds at 720p HD resolution).
|*INPUT : PYTHONAPI TO FEED INPUT* | IndexFaces | C# DetectWithUrlAsync() |
|*OUTPUT VALUES* | Gives us - | Gives us -|
| | - Confidence Level | - Gives various landmarks of face like, nose, pupil etc. |
| | - Emotions| - Emotions |
| | - Age | - Age |
| | - Gender | - Gender |
| | - Bounding coordinates of given image | - Gives the location and dimensions of faces |
|*OUTPUT FORMAT* | JSON | JSON |
